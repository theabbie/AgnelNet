import React, { Component } from 'react';
import { Text, View, Picker } from 'react-native';

export class BranchSelect extends Component {
	render() {
		return (
			<View
				style={{
					marginTop: 8,
					backgroundColor: '#ccc',
					flexDirection: 'row',
					justifyContent: 'space-around',
					alignItems: 'center'
				}}
			>
				<Text style={{ fontSize: 18, paddingBottom: 3, paddingLeft: 10 }}>
					Branch:{' '}
				</Text>
				<Picker {...this.props} style={[this.props.style]}>
					<Picker.Item label='Computer' value='Computer' />
					<Picker.Item label='Mechanical' value='Mechanical' />
					<Picker.Item label='IT' value='IT' />
					<Picker.Item label='Electrical' value='Electrical' />
					<Picker.Item label='EXTC' value='EXTC' />
				</Picker>
			</View>
		);
	}
}

export default BranchSelect;
