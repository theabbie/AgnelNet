import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ValidationComponent from 'react-native-form-validator';

const NameInput = props => {
	return (
		<View style={styles.inputContainer}>
			<Input
				{...props}
				placeholder='Full Name'
				label='Name: '
				leftIcon={
					<Icon
						name='account'
						size={24}
						color='black'
						style={{ paddingRight: 10 }}
					/>
				}
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	inputContainer: {
		margin: 8
	}
});

export default NameInput;
