import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const PasswordInput = props => {
	return (
		<View style={([styles.inputContainer], { marginTop: 10 })}>
			<Input
				{...props}
				secureTextEntry={true}
				placeholder='Password'
				label='Password: '
				leftIcon={
					<Icon
						name='textbox-password'
						size={24}
						color='black'
						style={{ paddingRight: 10 }}
					/>
				}
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	inputContainer: {
		margin: 8
	}
});

export default PasswordInput;
