import React, { Component } from 'react';
import { View, StyleSheet, Keyboard } from 'react-native';
import { Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import DateTimePicker from '@react-native-community/datetimepicker';

class DOBPicker extends Component {
	constructor(props) {
		super(props);
		this.state = {
			date: new Date('2001-01-01'),
			mode: 'date',
			show: false
		};
	}
	setDate = (event, date) => {
		date = date || this.state.date;
		this.setState({
			show: Platform.OS === 'ios' ? true : false,
			date
		});
		this.props.handleChangeDate(date);
		Keyboard.dismiss();
	};

	show = mode => {
		this.setState({
			show: true,
			mode
		});
	};

	datepicker = () => {
		this.show('date');
	};

	timepicker = () => {
		this.show('time');
	};

	render() {
		const { show, date, mode } = this.state;

		return (
			<View style={styles.inputContainer}>
				{show && (
					<DateTimePicker
						value={date}
						mode={mode}
						locale='es-ES'
						minimumDate={new Date('1992-01-01')}
						maximumDate={new Date('2004-01-01')}
						onChange={this.setDate}
					/>
				)}

				<Input
					value={this.state.date.toLocaleDateString()}
					label='Date Of Birth: '
					placeholder='Date'
					onTouchStart={() => this.datepicker()}
					leftIcon={
						<Icon
							name='cake'
							size={24}
							color='black'
							style={{ paddingHorizontal: 10 }}
						/>
					}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	signupContainer: {
		flex: 1,
		backgroundColor: '#ddd',
		alignItems: 'center'
	}
});

export default DOBPicker;
