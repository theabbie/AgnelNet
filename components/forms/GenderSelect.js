import React, { Component } from 'react';
import { Text, View, Picker } from 'react-native';

export class GenderSelect extends Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<View
				style={{
					marginVertical: 8,
					backgroundColor: '#ccc',
					flexDirection: 'row',
					justifyContent: 'space-around',
					alignItems: 'center'
				}}
			>
				<Text style={{ fontSize: 18, paddingBottom: 3, paddingLeft: 10 }}>
					Gender:{' '}
				</Text>
				<Picker {...this.props} style={[this.props.style]}>
					<Picker.Item label='Male' value='M' />
					<Picker.Item label='Female' value='F' />
					<Picker.Item label='Others' value='O' />
				</Picker>
			</View>
		);
	}
}

export default GenderSelect;
