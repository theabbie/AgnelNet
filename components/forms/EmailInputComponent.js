import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const EmailInput = props => {
	return (
		<View style={styles.inputContainer}>
			<Input
				{...props}
				keyboardType='email-address'
				placeholder='Email'
				label='Email: '
				leftIcon={
					<Icon
						name='email'
						size={24}
						color='black'
						style={{ paddingRight: 10 }}
					/>
				}
			/>
		</View>
	);
};
const styles = StyleSheet.create({
	inputContainer: {
		margin: 8
	}
});

export default EmailInput;
