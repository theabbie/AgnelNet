import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

class AddressInput extends React.Component {
	constructor(props) {
		super(props);
	}
	handleChangeText = text => {
		this.props.onChangeText(text);
	};
	render() {
		return (
			<View style={styles.inputContainer}>
				<Input
					{...this.props}
					onChangeText={this.handleChangeText}
					placeholder='Full Address'
					label='Address: '
					leftIcon={
						<Icon
							name='map-marker'
							size={24}
							color='black'
							style={{ paddingRight: 10 }}
						/>
					}
				/>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	inputContainer: {
		margin: 8
	}
});

export default AddressInput;
