import React from 'react';
import {
	View,
	Text,
	StyleSheet,
	TouchableOpacity,
	TouchableNativeFeedback,
	Platform
} from 'react-native';

const DefaultButton = props => {
	if (Platform.OS === 'ios') {
		return (
			<TouchableOpacity onPress={props.onPress}>
				<View style={[styles.defaultButton, props.buttonStyle]}>
					<Text style={[{ color: '#fff' }, props.TextStyle]}>
						{props.title}
					</Text>
				</View>
			</TouchableOpacity>
		);
	}
	return (
		<TouchableNativeFeedback
			background={TouchableNativeFeedback.Ripple('#fff')}
			useForeground={true}
			onPress={props.onPress}
		>
			<View style={[styles.defaultButton, props.buttonStyle]}>
				<Text style={[{ color: '#fff' }, props.TextStyle]}>
					{props.title.toUpperCase()}
				</Text>
			</View>
		</TouchableNativeFeedback>
	);
};
const styles = StyleSheet.create({
	defaultButton: {
		width: '100%',
		height: 31,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#000',
		marginTop: 7,
		borderRadius: 2,
		elevation: 3
	}
});

export default DefaultButton;
