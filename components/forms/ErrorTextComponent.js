import React from 'react';
import { View } from 'react-native';
import { Text } from 'react-native-elements';

const ErrorText = props => {
	return (
		<View
			style={{
				backgroundColor: '#ccc',
				height: 35,
				justifyContent: 'center',
				opacity: props.opacity
			}}
		>
			{props.errors.map((error, index) => {
				return (
					<Text
						numberOfLines={1}
						style={{ fontSize: 10, alignSelf: 'center', color: '#f00' }}
						key={index}
					>
						{error.fieldName}:{error.messages.map(msg => msg)}
					</Text>
				);
			})}
		</View>
	);
};

export default ErrorText;
