import React from 'react';
import { Text, View } from 'react-native';
import { ListItem } from 'react-native-elements';

const SocialInfo = props => {
	return (
		<View style={{ marginTop: 20 }}>
			<View style={{ width: '88%', alignSelf: 'center' }}>
				<Text style={{ fontSize: 20 }}>Social Info:</Text>
			</View>
			<View
				style={{
					width: '100%',
					justifyContent: 'center',
					alignItems: 'center'
				}}
			>
				<View style={{ marginTop: 10, width: '88%' }}>
					{props.socialInfo.map(item => {
						return (
							<ListItem
								title={`${item.name}: ${item.username}`}
								bottomDivider
								key={item.username.toString()}
							/>
						);
					})}
				</View>
			</View>
		</View>
	);
};

export default SocialInfo;
