import React from 'react';
import {
	View,
	Text,
	Button,
	StyleSheet,
	Platform,
	TouchableNativeFeedback,
	TouchableHighlight,
	TouchableOpacity
} from 'react-native';
import { Avatar } from 'react-native-elements';

import DefaultButton from '../forms/DefaultButton';

const NameCard = props => {
	return (
		<View
			style={{
				flexDirection: 'row'
			}}
		>
			<View style={{ marginLeft: 20 }}>
				<Avatar source={{ uri: props.user.profileImg }} size={100} />
			</View>
			<View style={{ marginLeft: 25, marginTop: 20 }}>
				<Text style={{ fontSize: 17 }}>{props.user.name}</Text>
				<Text style={{ color: '#666', fontSize: 12 }}>{props.user.email}</Text>
				{props.canEdit && (
					<DefaultButton
						title='Edit Button'
						onPress={props.onEditButtonPress}
					/>
				)}
			</View>
		</View>
	);
};

const styles = StyleSheet.create({});

export default NameCard;
