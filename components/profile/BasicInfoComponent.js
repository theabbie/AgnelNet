import React from 'react';
import { View,Text } from 'react-native';
import { ListItem } from 'react-native-elements'

const BasicInfo = props => {
	return (
		<View style={{ marginTop: 20 }}>
			<View style={{ width: '88%', alignSelf: 'center' }}>
				<Text style={{ fontSize: 20 }}>Basic Info:</Text>
			</View>
			<View
				style={{
					width: '100%',
					justifyContent: 'center',
					alignItems: 'center'
				}}
			>
				<View style={{ marginTop: 10, width: '88%' }}>
					{props.basicInfo.map(item => {
						return (
							<ListItem
								title={`${item.label}: ${item.value}`}
								bottomDivider
								key={item.value.toString()}
							/>
						);
					})}
				</View>
			</View>
		</View>
	);
};

export default BasicInfo;