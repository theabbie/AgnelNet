import React from 'react';
import { AsyncStorage, View, ActivityIndicator } from 'react-native';
import { NavigationNativeContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { ThemeProvider } from 'react-native-elements';

import IntroStack from './navigations/IntroStack';
import MainTab from './navigations/MainTab';
import MappView from './screens/MapViewScreen';
import LoggedIn from './Login';
import { createRootNavigator } from './navigations/RootSwitch';
import { isSignedIn } from './auth';

// const isLoggedIn = () => {
// 	let _id = false;
// 	let _token = false;
// 	// AsyncStorage.getItem('_id', (err, result) => {
// 	// 	if (err) {
// 	// 		alert(err);
// 	// 		return false;
// 	// 	}
// 	// 	if (result) {
// 	// 		console.log(result);
// 	// 		AsyncStorage.getItem('token', (err, result) => {
// 	// 			if (err) {
// 	// 				alert(err);
// 	// 				return false;
// 	// 			}
// 	// 			if (result) {
// 	// 				return true;
// 	// 			}
// 	// 		});
// 	// 	}
// 	// });
// 	// AsyncStorage.removeItem('_id', err => {
// 	// 	if (err) console.log(err);
// 	// });
// 	// AsyncStorage.getItem('_id', (err, result) => {
// 	// 	if (err) return console.log(err);
// 	// 	if (result) return console.log(result);
// 	// });
// 	AsyncStorage.getItem('_id', (err, result) => {
// 		if (result) {
// 			_id = true;
// 		}
// 		console.log(result);
// 	});

// 	AsyncStorage.getItem('token', (err, _result) => {
// 		if (_result) {
// 			_token = true;
// 		}
// 		console.log(_result);
// 	});
// 	if (_id) {
// 		return true;
// 	}
// };

export default class App extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		const Layout = createRootNavigator();
		return <Layout />;
	}
}
