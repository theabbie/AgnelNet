import React from 'react';
import { Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { createBottomTabNavigator } from 'react-navigation-tabs';
// import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationNativeContainer } from '@react-navigation/native';

import MainStack from '../navigations/MainStack';
import SettingStack from '../navigations/SettingStack';
import MainScreen from '../screens/MainScreen';
import ProfileScreen from '../screens/ProfileScreen';
import SettingScreen from '../screens/SettingSreen';

const MainTab = createBottomTabNavigator(
	{
		MainStack: {
			screen: MainStack,
			navigationOptions: {
				tabBarLabel: 'Home',
				tabBarIcon: ({ color, size }) => (
					<Icon name='home' color={color} size={size} />
				)
			}
		},
		SettingStack: {
			screen: SettingStack,
			navigationOptions: {
				tabBarLabel: 'Setting',
				tabBarIcon: ({ color, size }) => (
					<Icon name='account' color={color} size={size} />
				)
			}
		}
	},
	{ tabBarOptions: { activeTintColor: '#111' }, initialRouteName: 'MainStack' }
);

export default MainTab;
