import {
	StackNavigator,
	TabNavigator,
	SwitchNavigator
} from 'react-navigation';

import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import MainTab from './MainTab';
import IntroStack from './IntroStack';
import AuthLoadingScreen from '../screens/AuthLoadingScreen';

export const createRootNavigator = (signedIn = false) => {
	const RootSwitch = createSwitchNavigator(
		{
			Main: {
				screen: MainTab
			},
			Intro: {
				screen: IntroStack
			},
			AuthLoading: {
				screen: AuthLoadingScreen
			}
		},
		{
			initialRouteName: 'AuthLoading'
		}
	);
	return createAppContainer(RootSwitch);
};
