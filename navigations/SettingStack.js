import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { createStackNavigator } from 'react-navigation-stack';

import SettingScreen from '../screens/SettingSreen';
import EditProfileScreen from '../screens/EditProfileScreen';
import BarcodeScanScreen from '../screens/BarcodeScanScreen';
import AddSocialScreen from '../screens/AddSocialScreen';

const SettingStack = createStackNavigator({
	Setting: { screen: SettingScreen },
	EditProfile: {
		screen: EditProfileScreen,
		navigationOptions: ({ navigation }) => {
			return {
				headerRight: () => (
					<TouchableOpacity onPress={navigation.getParam('saveButtonClick')}>
						<Icon name='check' size={33} style={{ marginRight: 10 }} />
					</TouchableOpacity>
				)
			};
		}
	},
	AddSocial: { screen: AddSocialScreen },
	BcodeScan: {
		screen: BarcodeScanScreen,
		navigationOptions: { headerShown: false }
	}
});

export default SettingStack;
