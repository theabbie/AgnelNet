import React from 'react';
import { NavigationNativeContainer } from '@react-navigation/native';
import { createStackNavigator } from 'react-navigation-stack';
// import { createStackNavigator } from '@react-navigation/stack';

import MainScreen from '../screens/MainScreen';
import ProfileScreen from '../screens/ProfileScreen';

const MainStack = createStackNavigator({
	Main: { screen: MainScreen, navigationOptions: { headerShown: false } },
	Profile: { screen: ProfileScreen }
});

export default MainStack;
