import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { NavigationNativeContainer } from '@react-navigation/native';
import { createStackNavigator } from 'react-navigation-stack';
// import { createStackNavigator } from '@react-navigation/stack';

import IntroScreen from '../screens/IntroScreen';
import SignupScreen from '../screens/SignupScreen';
import LoginScreen from '../screens/LoginScreen';

import BarcodeScanScreen from '../screens/BarcodeScanScreen';
const IntroStack = createStackNavigator({
	Intro: { screen: IntroScreen, navigationOptions: { headerShown: false } },
	Signup: {
		screen: SignupScreen,
		navigationOptions: {
			headerStyle: { backgroundColor: '#444' },
			headerTitleAlign: 'center',
			headerTintColor: '#eee'
		}
	},
	Login: {
		screen: LoginScreen,
		navigationOptions: {
			headerStyle: { backgroundColor: '#444' },
			headerTitleAlign: 'center',
			headerTintColor: '#eee'
		}
	},
	BcodeScan: {
		screen: BarcodeScanScreen,
		navigationOptions: { headerShown: false }
	}
});

// export class IntroStack extends Component {
// 	constructor(props) {
// 		super(props);
// 	}

// 	renderMain = () => {
// 		this.props.navigation.navigate('Main');
// 	};
// 	render() {
// 		return (
// 			<NavigationNativeContainer>
// 				<Stack.Navigator>
// 					<Stack.Screen
// 						name='Intro'
// 						component={IntroScreen}
// 						options={{ headerShown: false }}
// 					/>
// 					<Stack.Screen
// 						name='Signup'
// 						component={SignupScreen}
// 						options={{
// 							renderMain: this.renderMain,
// 							headerStyle: {
// 								backgroundColor: '#444'
// 							},
// 							headerTitleAlign: 'center',
// 							headerTintColor: '#eee'
// 						}}
// 					/>
// 					<Stack.Screen
// 						renderMain={this.renderMain}
// 						name='Login'
// 						component={LoginScreen}
// 						options={{
// 							headerStyle: {
// 								backgroundColor: '#444'
// 							},
// 							headerTitleAlign: 'center',
// 							headerTintColor: '#eee'
// 						}}
// 					/>
// 					<Stack.Screen
// 						name='BcodeScan'
// 						component={BarcodeScanScreen}
// 						options={{ headerShown: false }}
// 					/>
// 				</Stack.Navigator>
// 			</NavigationNativeContainer>
// 		);
// 	}
// }

export default IntroStack;
