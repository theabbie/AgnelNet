import React, { Component, useState } from 'react';
import {
	View,
	Button,
	Modal,
	TouchableHighlight,
	Alert,
	ScrollView,
	StyleSheet,
	Platform,
	Keyboard
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Text, Input } from 'react-native-elements';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import ValidationComponent from 'react-native-form-validator';

import KeyboardShift from '../helpers/KeyboardShift';

import NameInput from '../components/forms/NameInputComponent';
import EmailInput from '../components/forms/EmailInputComponent';
import DOBPicker from '../components/forms/DOBPickerComponent';
import BranchSelect from '../components/forms/BranchSelectComponent';
import GenderSelect from '../components/forms/GenderSelect';
import AddressInput from '../components/forms/AddressInputComponent';
import PasswordInput from '../components/forms/PasswordInputComponent';
import ErrorText from '../components/forms/ErrorTextComponent';

const SERVER_URL = "http://192.168.43.194:8000/"

const GetLocationButton = props => {
	return (
		<View>
			<Button {...props} color='#444' title='Get Location' />
		</View>
	);
};

const GetBarcodeDetailButton = props => {
	return (
		<View style={{ marginTop: 20 }}>
			<Button {...props} title='Scan Barcode' color='#444' />
		</View>
	);
};

const RollNoText = props => {
	return (
		<View style={{ backgroundColor: '#ccc', padding: 7, marginTop: 10 }}>
			<Text style={{ fontSize: 20, alignSelf: 'center' }}>
				Roll No: {props.rollNo}
			</Text>
		</View>
	);
};

const SubmitButton = props => {
	return (
		<View style={{ width: '50%', alignSelf: 'center', marginVertical: 25 }}>
			<Button {...props} title='Submit' color='#333' />
		</View>
	);
};

class SignupScreen extends ValidationComponent {
	constructor(props) {
		super(props);
		this.state = this.props.navigation.state.BcodeData || {
			name: 'Gaurav Khaire',
			email: 'gkhaire123@gmail.com',
			password: '___gkhaire14',
			rollNo: 1019198,
			branch: 'IT',
			address: 'billage road cottaageeee',
			dob: new Date('1998-01-01'),
			gender: 'M',
			addressCoords: {
				lat: 19.0040,
				lng: 72.0010
			},
			barcodeScanned: false
		};
	}
	/*
	getLocation = async () => {
		let { status } = await Permissions.askAsync(Permissions.LOCATION);
		if (status !== 'granted') {
			alert('Permission  Denied for Location');
		} else {
			try {
				let location = await Location.getCurrentPositionAsync({});
				alert(
					`Coordinates lat: ${location.coords.longitude}, long: ${location.coords.latitude}`
				);
				this.setState({
					addressCoords: {
						lat: location.coords.latitude,
						lng: location.coords.longitude
					}
				});
			} catch (err) {
				alert(`Try again please: ${err}`);
			}
		}
	};
*/
	getLocation = () => {
		return Permissions.askAsync(Permissions.LOCATION)
			.then(({ status }) => {
				console.log(status);
				if (status !== 'granted') {
					alert('Permission  Denied for Location');
				} else if (status === 'granted') {
					Location.getCurrentPositionAsync({})
						.then(location => {
							alert(
								`Coordinates lat: ${location.coords.longitude}, long: ${location.coords.latitude}`
							);
							this.setState({
								addressCoords: {
									lat: location.coords.latitude,
									lng: location.coords.longitude
								}
							});
						})
						.catch(err => alert(`inerr: ${err}`));
				}
			})
			.catch(err => alert(`outerr: ${err}`));
	};

	

	handleSubmit = () => {
		this.validate({
			name: { minlength: 3, maxlength: 30, required: true },
			email: { email: true, required: true },
			password: { minlength: 8, maxlength: 50, required: true },
			rollNo: { numbers: true, required: true },
			dob: { date: 'MM-DD-YY', required: true },
			address: { minlength: 10, maxlength: 75 },
			gender: { required: true },
			branch: { required: true }
		});
		if (this.errors.length < 1) {
			const data = {
				name: this.state.name,
				email: this.state.email,
				password: this.state.password,
				rollNo: this.state.rollNo,
				branch: this.state.branch,
				dob: this.state.dob,
				gender: this.state.gender,
				address: this.state.address,
				addressCoords: this.state.addressCoords
			};
			// console.log(data);

			fetch(SERVER_URL +'api/users/register/', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify(data)
			})
				.then(response =>  response.json())
				.then(responseJson => {
					this.props.navigation.navigate('Login')
				})
				.catch(err => console.log(err));
		}
	};

	render() {
		return (
			<KeyboardShift>
				<ScrollView style={{ backgroundColor: '#ddd' }}>
					<View style={styles.signupContainer}>
						<View style={styles.formContainer}>
							<ErrorText
								errors={this.errors}
								opacity={this.errors.length >= 1 ? 1 : 0}
							/>

							<NameInput
								value={this.state.name}
								onChangeText={name => {
									this.setState({ name: name });
									this.validate({
										name: { minlength: 3, maxlength: 30 }
									});
								}}
							/>
							<EmailInput
								value={this.state.email}
								onChangeText={email => {
									this.setState({ email: email });
									this.validate({ email: { email: true } });
								}}
							/>
							<DOBPicker ref='dob' handleChangeDate={(dob)=> this.setState({dob:dob})} />
							<BranchSelect
								selectedValue={this.state.branch}
								style={{ height: 50, width: 150 }}
								onValueChange={(itemValue, itemIndex) =>
									this.setState({ branch: itemValue })
								}
							/>
							<GenderSelect
								selectedValue={this.state.gender}
								style={{ height: 50, width: 150 }}
								onValueChange={(itemValue, itemIndex) =>
									this.setState({ gender: itemValue })
								}
							/>
							<AddressInput
								value={this.state.address}
								onChangeText={address => {
									this.setState({ address: address });
									this.validate({ address: { minlength: 10, maxlength: 75 } });
								}}
							/>
							<Text style={{ fontSize: 20, alignSelf: 'center', margin: 10 }}>
								OR
							</Text>
							<GetLocationButton
								value={this.state.addressCoords}
								onPress={this.getLocation}
							/>
							<GetBarcodeDetailButton
								onPress={() =>
									this.props.navigation.navigate('BcodeScan', {
										data: this.state,
										nextRoute: 'Signup'
									})
								}
							/>
							{this.state.barcodeScanned && (
								<RollNoText rollNo={this.state.rollNo} />
							)}
							<PasswordInput
								value={this.state.password}
								onChangeText={text => {
									this.setState({ password: text });
									this.validate({ password: { minlength: 8, maxlength: 50 } });
								}}
							/>
							<SubmitButton onPress={this.handleSubmit} />
						</View>
					</View>
				</ScrollView>
			</KeyboardShift>
		);
	}
}

const styles = StyleSheet.create({
	signupContainer: {
		flex: 1,
		backgroundColor: '#ddd',
		alignItems: 'center'
	},
	formContainer: {
		width: '85%',
		marginTop: 25
	},
	inputContainer: {
		margin: 8
	}
});

export default SignupScreen;
