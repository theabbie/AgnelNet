import React, { Component } from 'react';
import { Text, StyleSheet, View, AsyncStorage } from 'react-native';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import SignupScreen from '../screens/SignupScreen';

export default class IntroScreen extends Component {
	constructor(props) {
		super(props);

		// AsyncStorage.getItem('_id', (err, result) => {
		// 	if (!err && result) {
		// 		AsyncStorage.getItem('token', (_err, _result) => {
		// 			if (!_err && _result) {
		// 				console.log(`${result}:${_result}`);
		// 				this.props.navigation.navigate('MainTab');
		// 			}
		// 		});
		// 	}
		// });
	}

	render() {
		return (
			<View style={styles.introContainer}>
				<View style={styles.brandNameContainer}>
					<Text style={styles.brandName}> Agnel Net </Text>
				</View>
				<View style={styles.buttonContainer}>
					<View style={{ flex: 1 }}>
						<Button
							icon={
								<Icon
									name='account-plus'
									size={25}
									style={{ marginRight: 10 }}
									color='#bbb'
								/>
							}
							type='clear'
							title='Signup'
							onPress={() => this.props.navigation.navigate('Signup')}
						/>
					</View>
					<View style={{ flex: 1 }}>
						<Button
							icon={
								<Icon
									name='login'
									size={25}
									style={{ marginRight: 10 }}
									color='#bbb'
								/>
							}
							type='clear'
							title='Login'
							onPress={() => this.props.navigation.navigate('Login')}
						/>
					</View>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	introContainer: {
		flex: 1,
		backgroundColor: '#222'
	},
	brandNameContainer: {
		flex: 14,
		paddingTop: 300,
		alignItems: 'center'
	},
	brandName: {
		color: '#fff',
		fontSize: 35
	},
	buttonContainer: {
		flex: 2,
		flexDirection: 'row',
		width: '100%'
	}
});
