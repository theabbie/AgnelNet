import React, { Component } from 'react';
import { Text, View, ActivityIndicator, AsyncStorage } from 'react-native';

import { isSignedIn } from '../auth';

export class AuthLoadingScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			signedIn: false,
			checkedSignIn: false
		};

		AsyncStorage.getItem('_id', (err, res) => console.log(res));
		AsyncStorage.getItem('Token', (err, res) => console.log(res));
	}

	componentDidMount() {
		isSignedIn()
			.then(res =>
				this.setState({ signedIn: res, checkedSignIn: true }, () => {
					if (this.state.signedIn) {
						this.props.navigation.navigate('Main');
					} else {
						this.props.navigation.navigate('Intro');
					}
				})
			)
			.catch(err => alert('An error occurred' + err));
	}
	render() {
		return (
			<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
				<ActivityIndicator size={70} />
			</View>
		);
	}
}
export default AuthLoadingScreen;
