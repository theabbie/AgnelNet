import React, { Component } from 'react';
import {
	Text,
	View,
	StyleSheet,
	Dimensions,
	ActivityIndicator
} from 'react-native';
import MapView, { Marker } from 'react-native-maps';

import data from '../data';

const SERVER_URL = 'http://192.168.43.194:8000/';

const MarkerColors = {
	Computer: 'yellow',
	Mechanical: 'green',
	EXTC: 'orange',
	Electrical: 'blue',
	IT: 'purple'
};

const Mumbai = {
	lat: 19.076,
	lng: 72.877
};

const Agnel = {
	lat: 19.0755,
	lng: 72.9913
};



export class MainScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: true,
			users: []
		};
	}

	componentDidMount() {
		return fetch('http://192.168.43.194:8000/api/users/', {
			method: 'GET',
			headers: { 'Content-Type': 'application/json' }
		})
			.then(response => response.json())
			.then(responseJson => {
				this.setState({ isLoading: false, users: responseJson });
			})
			.catch(err => console.error(err));
	}

	render() {
		if (this.state.isLoading) {
			return (
				<View
					style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
				>
					<ActivityIndicator size={70} />
				</View>
			);
		}
		return (
			<View style={styles.container}>
				<MapView
					style={styles.mapStyle}
					region={{
						latitude: Mumbai.lat,
						longitude: Mumbai.lng,
						latitudeDelta: 1,
						longitudeDelta: 1
					}}
				>
					<Marker
						coordinate={{ latitude: Agnel.lat, longitude: Agnel.lng }}
						title='FCRIT'
						style={{ width: 100, height: 100 }}
						description='Central Jail of Vashi'
						pinColor='aqua'
					/>
					{this.state.users.map((user, id) => (
						<Marker
							key={user.rollNo}
							identifier={user.rollNo.toString()}
							coordinate={{
								latitude: user.addressCoords.lat,
								longitude: user.addressCoords.lng
							}}
							pinColor={MarkerColors[user.branch]}
							title={`${user.name}(${user.branch})`}
							description={`View Profile`}
							onCalloutPress={() =>
								this.props.navigation.navigate('Profile', { id: user._id })
							}
						></Marker>
					))}
				</MapView>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		marginTop: 23,
		backgroundColor: '#ddd'
	},
	mapStyle: {
		width: Dimensions.get('window').width,
		height: Dimensions.get('window').height
	}
});

export default MainScreen;
