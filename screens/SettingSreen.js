import React, { Component } from 'react';
import {
	Text,
	View,
	StyleSheet,
	Button,
	AsyncStorage,
	ActivityIndicator,
	ScrollView
} from 'react-native';
import { Constants } from 'expo';

import NameCard from '../components/profile/NameCardComponent';
import BasicInfo from '../components/profile/BasicInfoComponent';
import SocialInfo from '../components/profile/SocialInfoComponent';
import DefaultButton from '../components/forms/DefaultButton';

import { onSignOut } from '../auth';

export class SettingSreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: true,
			_id: '',
			token: '',
			user: null,
			basicInfo: []
		};
	}
	async componentDidMount() {
		try {
			let _id = await AsyncStorage.getItem('_id');
			let token = await AsyncStorage.getItem('token');
			const response = await fetch(
				`http://192.168.43.194:8000/api/users/${_id}/`,
				{ method: 'GET', headers: { 'Content-Type': 'application/json' } }
			);
			const responseJson = await response.json();
			this.setState({
				isLoading: false,
				_id: _id,
				token: token,
				user: responseJson,
				basicInfo: [
					{
						label: 'branch',
						value: responseJson.branch
					},
					{
						label: 'rollNo',
						value: responseJson.rollNo.toString()
					},
					{
						label: 'DOB',
						value: new Date(responseJson.dob).toLocaleDateString()
					},

					{
						label: 'Gender',
						value: responseJson.gender
					}
				]
			});
		} catch (err) {
			if (err) {
				alert(err);
				this.props.navigation.navigate('AuthLoading');
			}
		}
	}

	handleSignOut = () => {
		onSignOut()
			.then(() => {
				this.props.navigation.navigate('AuthLoading');
			})

			.catch(err => alert('Please try again !'));
	};
	render() {
		if (this.state.isLoading) {
			return (
				<View
					style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
				>
					<ActivityIndicator size={70} />
				</View>
			);
		}
		return (
			<ScrollView>
				<View style={styles.container}>
					<NameCard
						user={this.state.user}
						canEdit={true}
						onEditButtonPress={() =>
							this.props.navigation.navigate('EditProfile', {
								user: this.state.user
							})
						}
					/>
					<BasicInfo basicInfo={this.state.basicInfo} />
					{this.state.user.social.length >= 1 && (
						<SocialInfo socialInfo={this.state.user.social} />
					)}
					<View style={styles.signOutButton}>
						<DefaultButton
							color='#000'
							title='sign out'
							onPress={this.handleSignOut}
						/>
					</View>
				</View>
			</ScrollView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 20
	},
	signOutButton: {
		width: '45%',
		alignSelf: 'center',
		marginTop: 20
	}
});

export default SettingSreen;
