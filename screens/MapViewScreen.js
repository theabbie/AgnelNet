import React from 'react';
import MapView from 'react-native-maps';
import { StyleSheet, Text, View, Dimensions } from 'react-native';
import { Marker } from 'react-native-maps';
import data from '../data';

const getUsers = () => {
	return data;
};

export default class MappView extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			users: []
		};
	}

	loadData = () => {
		let users = getUsers();
		users = JSON.parse(users);
	};

	componentDidMount() {
		// this.loadData();
		this.setState({ users: JSON.parse(getUsers()) });
		console.log(this.state.users[0].addressCoords);
	}

	render() {
		return (
			<View style={styles.container}>
				<MapView style={styles.mapStyle}>
					<Marker coordinate={{ latitude: 19.0, longitude: 72.0 }} />
				</MapView>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center'
	},
	mapStyle: {
		width: Dimensions.get('window').width,
		height: Dimensions.get('window').height
	}
});
