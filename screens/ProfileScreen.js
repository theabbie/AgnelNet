import React, { Component } from 'react';
import {
	Text,
	View,
	FlatList,
	SafeAreaView,
	SectionList,
	ActivityIndicator,
	ScrollView,
	YellowBox
} from 'react-native';
import { Avatar, ListItem, Card } from 'react-native-elements';
import data from '../data';
import { useSafeArea } from 'react-native-safe-area-context';

import BasicInfo from '../components/profile/BasicInfoComponent';
import SocialInfo from '../components/profile/SocialInfoComponent';
import NameCard from '../components/profile/NameCardComponent';

const SERVER_URL = 'http://192.168.43.194:8000/';

export class ProfileScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: true,
			user: {},

			basicInfo: []
		};
	}

	componentDidMount() {
		return fetch(
			`http://192.168.43.194:8000/api/users/${this.props.navigation.state.params.id}/`
		)
			.then(response => response.json())
			.then(responseJson => {
				this.setState({
					isLoading: false,
					user: responseJson,
					basicInfo: [
						{
							label: 'branch',
							value: responseJson.branch
						},
						{
							label: 'rollNo',
							value: responseJson.rollNo.toString()
						},
						{
							label: 'DOB',
							value: new Date(responseJson.dob).toLocaleDateString()
						},

						{
							label: 'Gender',
							value: responseJson.gender
						}
					]
				});
				this.props.navigation.setParams({ user: responseJson });
			})
			.catch(err => alert(err));
	}

	render() {
		if (this.state.isLoading) {
			return (
				<View
					style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
				>
					<ActivityIndicator size={70} />
				</View>
			);
		}

		return (
			<ScrollView style={{ flex: 1 }}>
				<View style={{ flex: 1, marginTop: 30 }}>
					<NameCard user={this.state.user} />
					<BasicInfo basicInfo={this.state.basicInfo} />

					{this.state.user.social.length >= 1 && (
						<SocialInfo socialInfo={this.state.user.social} />
					)}
				</View>
			</ScrollView>
		);
	}
}

export default ProfileScreen;
