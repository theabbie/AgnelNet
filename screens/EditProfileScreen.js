import React, { Component } from 'react';
import { Text, View, ScrollView, Button, StyleSheet } from 'react-native';
import { Avatar } from 'react-native-elements';
import { NavigationActions } from 'react-navigation';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';

import ValidationComponent from 'react-native-form-validator';
import KeyboardShift from '../helpers/KeyboardShift';

import NameInput from '../components/forms/NameInputComponent';
import EmailInput from '../components/forms/EmailInputComponent';
import DOBPicker from '../components/forms/DOBPickerComponent';
import BranchSelect from '../components/forms/BranchSelectComponent';
import GenderSelect from '../components/forms/GenderSelect';
import AddressInput from '../components/forms/AddressInputComponent';
import PasswordInput from '../components/forms/PasswordInputComponent';
import ErrorText from '../components/forms/ErrorTextComponent';
import DefaultButton from '../components/forms/DefaultButton';

const SERVER_URL = 'http://192.168.43.194:8000/';

const GetLocationButton = props => {
	return (
		<View>
			<DefaultButton
				{...props}
				buttonStyle={{ backgroundColor: '#444', height: 35 }}
				title='Get Location'
			/>
		</View>
	);
};

const GetBarcodeDetailButton = props => {
	return (
		<View style={{ marginTop: 7 }}>
			<DefaultButton
				{...props}
				title={props.title || 'Scan Barcode'}
				buttonStyle={{ backgroundColor: '#444', height: 35 }}
			/>
		</View>
	);
};

const RollNoText = props => {
	return (
		<View style={{ backgroundColor: '#ccc', padding: 7, marginTop: 10 }}>
			<Text style={{ fontSize: 20, alignSelf: 'center' }}>
				Roll No: {props.rollNo}
			</Text>
		</View>
	);
};

const AddSocialButton = props => {
	return (
		<View>
			<DefaultButton
				title='Add Social'
				{...props}
				buttonStyle={{ backgroundColor: '#444', height: 35 }}
			/>
		</View>
	);
};

export class EditProfileScreen extends ValidationComponent {
	constructor(props) {
		super(props);
		this.state = this.props.navigation.state.params.BcodeData || {
			...this.props.navigation.state.params.user,
			barcodeScanned: false
		};
	}
	submitButtonClick = () => {
		console.log(this.state.dob);
		this.validate({
			name: { minlength: 3, maxlength: 30, required: true },
			email: { email: true, required: true },
			rollNo: { numbers: true, required: true },
			dob: { date: 'YYYY-MM-DD', required: true },
			address: { minlength: 10, maxlength: 75 },
			gender: { required: true },
			branch: { required: true }
		});
		if (this.errors.length < 1) {
			const data = {
				name: this.state.name,
				email: this.state.email,
				rollNo: this.state.rollNo,
				branch: this.state.branch,
				dob: this.state.dob,
				gender: this.state.gender,
				address: this.state.address,
				addressCoords: this.state.addressCoords
			};

			fetch(
				SERVER_URL +
					`api/users/${this.props.navigation.state.params.user._id}/`,
				{
					method: 'PUT',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify(data)
				}
			)
				.then(response => response.json())
				.then(responseJson => {
					this.props.navigation.reset(
						[NavigationActions.navigate({ routeName: 'Setting' })],
						0
					);
				})
				.catch(err => console.log(err));
		}
	};
	componentDidMount() {
		this.props.navigation.setParams({
			saveButtonClick: this.submitButtonClick
		});
		//console.log(this.props.navigation)
	}
	getLocation = () => {
		return Permissions.askAsync(Permissions.LOCATION)
			.then(({ status }) => {
				console.log(status);
				if (status !== 'granted') {
					alert('Permission  Denied for Location');
				} else if (status === 'granted') {
					Location.getCurrentPositionAsync({})
						.then(location => {
							alert(
								`Coordinates lat: ${location.coords.longitude}, long: ${location.coords.latitude}`
							);
							this.setState({
								addressCoords: {
									lat: location.coords.latitude,
									lng: location.coords.longitude
								}
							});
						})
						.catch(err => alert(`inerr: ${err}`));
				}
			})
			.catch(err => alert(`outerr: ${err}`));
	};
	render() {
		return (
			<KeyboardShift>
				<ScrollView style={{ backgroundColor: '#ddd' }}>
					<View style={styles.container}>
						<View style={{ width: '100%' }}>
							<View style={{ alignSelf: 'center' }}>
								<Avatar size={100} source={{ uri: this.state.profileImg }} />
								<Text style={{ fontSize: 15, marginTop: 3 }}>
									Change Avatar
								</Text>
							</View>
							<View style={styles.signupContainer}>
								<View style={styles.formContainer}>
									<ErrorText
										errors={this.errors}
										opacity={this.errors.length >= 1 ? 1 : 0}
									/>

									<NameInput
										value={this.state.name}
										onChangeText={name => {
											this.setState({ name: name });
											this.validate({
												name: { minlength: 3, maxlength: 30 }
											});
										}}
									/>
									<EmailInput
										value={this.state.email}
										onChangeText={email => {
											this.setState({ email: email });
											this.validate({ email: { email: true } });
										}}
									/>
									<DOBPicker
										ref='dob'
										handleChangeDate={dob => this.setState({ dob: dob })}
									/>

									<BranchSelect
										selectedValue={this.state.branch}
										style={{ height: 50, width: 150 }}
										onValueChange={(itemValue, itemIndex) =>
											this.setState({ branch: itemValue })
										}
									/>
									<GenderSelect
										selectedValue={this.state.gender}
										style={{ height: 50, width: 150 }}
										onValueChange={(itemValue, itemIndex) =>
											this.setState({ gender: itemValue })
										}
									/>
									<AddressInput
										value={this.state.address}
										onChangeText={address => {
											this.setState({ address: address });
											this.validate({
												address: { minlength: 10, maxlength: 75 }
											});
										}}
									/>
									<GetLocationButton
										value={this.state.addressCoords}
										onPress={this.getLocation}
									/>
									<GetBarcodeDetailButton
										title='Change Roll No.'
										onPress={() =>
											this.props.navigation.navigate('BcodeScan', {
												data: this.state,
												nextRoute: 'EditProfile'
											})
										}
									/>
									<RollNoText rollNo={this.state.rollNo} />
									<AddSocialButton
										onPress={() => this.props.navigation.navigate('AddSocial')}
									/>
								</View>
							</View>
						</View>
					</View>
				</ScrollView>
			</KeyboardShift>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		marginTop: 20,
		marginBottom: 30
	},
	formContainer: {
		width: '85%',
		marginTop: 25
	},
	signupContainer: {
		flex: 1,
		backgroundColor: '#ddd',
		alignItems: 'center'
	}
});

export default EditProfileScreen;
