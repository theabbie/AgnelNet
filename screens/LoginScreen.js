import React from 'react';
import {
	View,
	Text,
	Button,
	TextInput,
	StyleSheet,
	AsyncStorage
} from 'react-native';
import {
	NavigationActions,
	SwitchActions,
	StackActions
} from 'react-navigation';
import ValidationComponent from 'react-native-form-validator';

//import App from '../App';

import EmailInput from '../components/forms/EmailInputComponent';
import PasswordInput from '../components/forms/PasswordInputComponent';
import ErrorText from '../components/forms/ErrorTextComponent';
import { onSignIn } from '../auth';

const SERVER_URL = 'http://192.168.43.194:8000/';

const resetAction = StackActions.reset({
	index: 0,
	key: null,
	actions: [NavigationActions.navigate({ routeName: 'Main' })]
});

const SubmitButton = props => {
	return (
		<View style={{ width: '50%', alignSelf: 'center', marginVertical: 25 }}>
			<Button {...props} title='Submit' color='#333' />
		</View>
	);
};

class LoginScreen extends ValidationComponent {
	constructor(props) {
		super(props);
		this.state = {
			email: 'rjoshh@gmail.com',
			password: '___joshua14'
		};
	}

	handleSubmit = () => {
		this.validate({
			email: { email: true, required: true },
			password: { minlength: 8, maxlength: 50, required: true }
		});
		if (this.errors.length < 1) {
			const data = {
				email: this.state.email,
				password: this.state.password
			};

			fetch('http://192.168.43.194:8000/api/users/login/', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify(data)
			})
				.then(response => response.json())
				.then(responseJson => {
					// console.log(responseJson);
					//Log User IN
					//Store Web Token in AsyncStorage

					onSignIn(responseJson.token, responseJson.user._id)
						.then(() => {
							this.props.navigation.navigate('Main');
						})
						.catch(err => {
							console.log(err);
							alert('Please Try Again!!');
						});
				})
				.catch(err => alert(err));
		}
	};

	render() {
		return (
			<View style={styles.loginContainer}>
				<View style={styles.formContainer}>
					<ErrorText
						errors={this.errors}
						opacity={this.errors.length >= 1 ? 1 : 0}
					/>
					<EmailInput
						value={this.state.email}
						onChangeText={email => {
							this.setState({ email: email });
							this.validate({ email: { email: true } });
						}}
					/>
					<PasswordInput
						value={this.state.password}
						onChangeText={text => {
							this.setState({ password: text });
							this.validate({ password: { minlength: 8, maxlength: 50 } });
						}}
					/>
					<SubmitButton onPress={this.handleSubmit} />
				</View>
			</View>
		);
	}
}

export default LoginScreen;

const styles = StyleSheet.create({
	loginContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#ddd'
	},
	formContainer: {
		width: '85%'
	}
});
