import { AsyncStorage } from 'react-native';

export const USER_KEY = 'token';
export const USER_ID = '_id';

export const onSignIn = (token, _id) => {
	return AsyncStorage.multiSet([
		[USER_KEY, token],
		[USER_ID, _id]
	]);
};

export const onSignOut = () => AsyncStorage.multiRemove([USER_KEY, USER_ID]);

export const isSignedIn = () => {
	return new Promise((resolve, reject) => {
		AsyncStorage.getItem(USER_KEY)
			.then(res => {
				if (res !== null) {
					resolve(true);
				} else {
					resolve(false);
				}
			})
			.catch(err => reject(err));
	});
};
