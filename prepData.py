import json
import random

file = open("data.json", 'r')
jsonData = json.load(file)

branchs = ["Mechanical", "Computer", "EXTC", "IT", "Electrical"] 

gender = "bottts"

for j in jsonData:
    if j["gender"] == "F":
        gender = "female"
    if j["gender"] == "M":
        gender = "male"

    j["profileImg"] = f"https://avatars.dicebear.com/v2/{gender}/{random.randint(1, 100000000)}.svg"

f = open('newData.json', 'w')
f.write(json.dumps(jsonData))
